// This is a program that converts a CHAR data type to an INT data type.

#include <stdio.h>

int main()
{
    char x;
    printf("Please enter character: ");
    scanf("%c", &x);
    printf("%i\n",x);

    int y;
    printf("Please enter an integer between 0-127: ");
    scanf("%i",&y);
    printf("%c\n",y);

    char mathe = 'T' + '\t';
    printf("T(69) + \\t(11) = %c(%d)\n", mathe, mathe);

    return 0;
}
