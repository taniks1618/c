#include "mpc.h"

#ifdef _WIN32

static char buffer[2048]

char* readline(char* prompt) {
    fputs(prompt, stdout);
    fputs(buffer, 2048, stdin);
    char* cpy = malloc(strlen(buffer)+1);
    strcpy(cpy, buffer);
    cpy[strlen(cpy)-1] = '\0';
    return cpy;
}

void add_history(char* unused) {}

#else
#include <editline/readline.h>
#include <histedit.h>
#endif

// create passible data types
enum { LERR_DIV_ZERO, LERR_BAD_OP, LERR_BAD_NUM };

// create LVAL = lisp val types
enum { LVAL_NUM, LVAL_ERR };

// declare the LVAL's
typedef struct {
    int type;
    long num;
    int err;
}   lval;

// Create a new number tpye LVAL
lval lval_num(long x) {
    lval v;
    v.type = LVAL_NUM;
    v.num = x;
    return v;
}

// Create LVAL error types
lval lval_err(int x) {
    lval v;
    v.type = LVAL_ERR;
    v.err = x;
    return v;
}

// print the lval
void lval_print(lval v) {
    switch (v.type) {
        // case type is num print it
        // the break out switch
        case LVAL_NUM: printf("%li", v.num); break;

        // in case the type is an error
        case LVAL_ERR:
            // check what type of error
            if (v.err == LERR_DIV_ZERO) {
                printf("ERROR: DIVISION BY A ZERO!");
            }
            if (v.err == LERR_BAD_OP) {
                printf("ERROR: INVALID OPERATOR!");
            }
            if (v.err == LERR_BAD_NUM) {
                printf("ERROR: INVALID NUMBER!");
            }
        break;
    }
}

// print LVAL followed by a new line
void lval_println(lval v) { lval_print(v); putchar('\n'); }

lval eval_op(lval x, char* op, lval y) {

    // if either value is an error return it
    if(x.type == LVAL_ERR) { return x; }
    if(y.type == LVAL_ERR) { return y; }

    // do math if not an error
    if (strcmp(op, "+") == 0) { return lval_num(x.num + y.num); }
    if (strcmp(op, "-") == 0) { return lval_num(x.num - y.num); }
    if (strcmp(op, "*") == 0) { return lval_num(x.num * y.num); }
    if (strcmp(op, "/") == 0) { 
        // if second operator is zero return ERRor
        return y.num == 0
            ? lval_err(LERR_DIV_ZERO)
            : lval_num(x.num / y.num);
    }

    return lval_err(LERR_BAD_OP);
}

lval eval(mpc_ast_t* t) {

    if (strstr(t->tag, "number")) {
        // check if there is any error in my conversion
        errno = 0;
        long x = strtol(t->contents, NULL, 10);
        return errno != ERANGE ? lval_num(x) : lval_err(LERR_BAD_NUM);
    }

    char* op = t->children[1]->contents;
    lval x = eval(t->children[2]);

    int i = 3;
    while (strstr(t->children[i]->tag, "expr")) {
        x = eval_op(x, op, eval(t->children[i]));
        i++;
    }

    return x;
}

int main (int argc, char** argv) {

    mpc_parser_t* Number = mpc_new("number");
    mpc_parser_t* Operator = mpc_new("operator");
    mpc_parser_t* Expr = mpc_new("expr");
    mpc_parser_t* CLisp = mpc_new("clisp");

    mpca_lang(MPCA_LANG_DEFAULT,
        "                                                       \
            number   : /-?[0-9]+/ ;                             \
            operator : '+' | '-' | '*' | '/' ;                  \
            expr     : <number> | '(' <operator> <expr>+ ')' ;  \
            lispy    : /^/ <operator> <expr>+ /$/ ;             \
        ",
        Number, Operator, Expr, CLisp);

    puts("CLisp Version 0.0.0.0.4");
    puts("Press Ctrl+c to exit\n");

    while (1) {

        char* input = readline("clisp-> ");
        add_history(input);

        mpc_result_t r;
        if (mpc_parse("<stdin>", input, CLisp, &r)) {
            lval result = eval(r.output);
            lval_println(result);
            mpc_ast_delete(r.output);
        } else {
            mpc_err_print(r.error);
            mpc_err_delete(r.error);
        }

        free(input);

    }

    mpc_cleanup(4, Number, Operator, Expr, CLisp);

    return 0;
}
