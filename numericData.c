#include <stdio.h>

int main()
{
    printf("HOW MANY DOGS: ");
    double doggos;

    scanf("%lf", &doggos);

    // 25000 doggos

    printf("%f\n%e\n%g\n", doggos, doggos, doggos);
    // Conversion Characters
    // %f - decimal
    // %e - scientific 
    // %g - computer decides
    return 0;
}
