// headers to include
#include <stdio.h>

// start the main part of the program
int main()
{
    printf("How many eggs did the chicken's lay today: "); // asks for data to collect

    int eggs; // starts the eggs functions
    scanf("%i", &eggs); // collect the data from human

    double dozen = (double) eggs / 12.0; // does the math 

    printf("You have %f dozen eggs. \n", dozen); // prints the end

    return 0; // finishes the program
}
