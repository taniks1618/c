# Data Types

## Numeric Data Types

### Integer (INT)

- Whole numbers only (1,2,5,-5)

### Double

- Can be halves of numbers with decimals

### Float

- Floating point (fractional point)

### Character (CHAR)

-  Any character from keyboard with single quotes.

## Primitive

- indivisible / atomic

### Arrays

- More than one thing inside of a group
1. int grade[]; // numbers
2. char balloon[]; // charters (single)
3. string cat[]; // has to end with \0 (NULL)

### Constant

- can be written like 69(INT), '69'(CHAR), and "69"(STRING)
1. int cats = 69;
               ^
               |
            Constant (INT)

## Complex

- Stucts

1. date today;
2. coords lol;


