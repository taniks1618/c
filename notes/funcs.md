# Functions

## Basic Program
- #include <stdio.h>
- int main()
- {
- int x = 6;
- printf("%i\n", x); \\ x = 6
- printf("wow \n");
- printf("what \n");
- printf("no \n");
- return 0;
- }
    - We can make the 3 printf statements a function.
